<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias_fotos".
 *
 * @property int $cod_noticia
 * @property int $cod_foto
 * @property int|null $visitas
 *
 * @property Fotos $codFoto
 * @property Noticias $codNoticia
 */
class NoticiasFotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias_fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_noticia', 'cod_foto'], 'required'],
            [['cod_noticia', 'cod_foto', 'visitas'], 'integer'],
            [['cod_noticia', 'cod_foto'], 'unique', 'targetAttribute' => ['cod_noticia', 'cod_foto']],
            [['cod_foto'], 'exist', 'skipOnError' => true, 'targetClass' => Fotos::className(), 'targetAttribute' => ['cod_foto' => 'codigo']],
            [['cod_noticia'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['cod_noticia' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_noticia' => 'Cod Noticia',
            'cod_foto' => 'Cod Foto',
            'visitas' => 'Visitas',
        ];
    }

    /**
     * Gets query for [[CodFoto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFoto()
    {
        return $this->hasOne(Fotos::className(), ['codigo' => 'cod_foto']);
    }

    /**
     * Gets query for [[CodNoticia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodNoticia()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'cod_noticia']);
    }
}

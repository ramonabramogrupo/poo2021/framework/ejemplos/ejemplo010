<?php
use yii\grid\GridView;
use \yii\helpers\Html;
use yii\bootstrap4\Carousel;
?>
<h1><?= $noticia->titulo ?></h1>
<div><?= $noticia->texto ?></div>

 <?php
            // en este array quiero colocar todas las fotos como img
            $elementos=[];
            foreach ($fotos as $foto){
                $elementos[]=Html::img("@web/imgs/" . $foto->nombre);
            }
        ?>
        <?=
        Carousel::widget([
         'items'=>$elementos,
         'options'=>[
            'class'=>'mx-auto col-lg-4'
         ],
            'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']
        ]);
            
        ?>



<h2 class="mt-5 mb-2">Comentarios de la noticia</h2>
<?php
echo GridView::widget([
    "dataProvider" => $dataProvider,
    "layout"=>"{items}",
    "columns" =>[
        "texto",
        "fecha",
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{editar} {eliminar}',
            'buttons' => [
                'editar' => function ($url,$model) {
                    return Html::a('<i class="far fa-edit"></i>', 
                       ['site/editarcomentario',"codigo"=>$model->codigo]
                       );
                },
                'eliminar' => function ($url,$model) {
                    return Html::a('<i class="fas fa-trash"></i>',
                            ['site/eliminarcomentario',"codigo"=>$model->codigo],
                            [
                              'data' => [
                                  'confirm' => '¿Seguro que deseas eliminar el comentario?',
                                  'method' => 'post',
                              ]  
                            ]);
                },
	        ],
        ],
    ]
]);


<?php
/* @var $this yii\web\View */

use yii\widgets\ListView;
use yii\helpers\Html;

$this->title = 'Noticias';

?>
<div class="site-index">
    <h1 class="border rounded bg-secondary p-3 text-white text-center mb-5">
        Noticias
    </h1>
    <div class="body-content">
        <div>
        <?=  
            Html::a("Añadir Noticia",
                ['site/nuevanoticia'],
                ['class'=>'btn btn-primary float-right']
            );
            
         ?>
            <br>
        </div>
        <br><br>
        
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_noticia',
                "itemOptions" => [
                    'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
                ],
                "options" => [
                    'class' => 'row',
                ],
                'layout' => "{items}"
            ]);
            ?>

        
    </div>
</div>
